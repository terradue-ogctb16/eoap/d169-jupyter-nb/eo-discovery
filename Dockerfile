FROM terradue/l0-binder:latest

MAINTAINER Terradue S.r.l



COPY . ${HOME}
USER root

#RUN /opt/anaconda/bin/conda env create -f ${HOME}/environment.yml

#RUN cat ${HOME}/requirements.txt) | tr '\n' ' '

#RUN /opt/anaconda/bin/conda install -y -c conda-forge -c terradue python=3.7 cioppy owslib

RUN /opt/anaconda/bin/conda env update --name base --file ${HOME}/environment.yml

RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}

WORKDIR ${HOME}